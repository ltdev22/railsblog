source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
  "https://github.com/#{repo_name}.git"
end

gem 'rails', '~> 5.0.4'
gem 'mysql2', '>= 0.3.18', '< 0.5'
gem 'puma', '~> 3.0'
gem 'sass-rails', '~> 5.0'
gem 'uglifier', '>= 1.3.0'
gem 'coffee-rails', '~> 4.2'

gem 'jquery-rails'
gem 'turbolinks', '~> 5'
gem 'jbuilder', '~> 2.5'

gem 'dotenv-rails', '~> 2.2', '>= 2.2.1'
gem 'bootstrap-sass', '~> 3.3', '>= 3.3.7'
gem 'font-awesome-sass', '~> 4.7'
gem 'haml', '~> 5.0', '>= 5.0.1'
gem 'haml-rails', '~> 1.0'
gem 'html2haml', '~> 2.2'
gem 'simple_form', '~> 3.5'
gem 'kaminari', '~> 1.0', '>= 1.0.1'
gem 'momentjs-rails', '~> 2.17', '>= 2.17.1'
gem 'bootstrap3-datetimepicker-rails', '~> 4.17', '>= 4.17.47'
gem 'jquery-turbolinks', '~> 2.1'
gem 'devise', '~> 4.3'
gem 'pundit', '~> 1.1'

group :development, :test do
  gem 'byebug', platform: :mri
  gem 'pry-rails', '~> 0.3.6'
  gem 'pry-byebug', '~> 3.4', '>= 3.4.2'
  gem 'rspec-rails', '~> 3.6'
  gem 'factory_girl_rails', '~> 4.8'
  gem 'faker', '~> 1.8', '>= 1.8.4'
  gem 'database_cleaner', '~> 1.6', '>= 1.6.1'
end

group :development do
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '~> 3.0.5'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'annotate', '~> 2.7', '>= 2.7.2'
  gem 'better_errors', '~> 2.1', '>= 2.1.1'
  gem 'bullet', '~> 5.5', '>= 5.5.1'
end

group :test do
  gem 'capybara', '~> 2.14', '>= 2.14.4'
  gem 'launchy', '~> 2.4', '>= 2.4.3'
  gem 'shoulda-matchers', '~> 3.1', '>= 3.1.2'
  gem 'pundit-matchers', '~> 1.3', '>= 1.3.1'
end

gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
