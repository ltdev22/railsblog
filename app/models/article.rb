# == Schema Information
#
# Table name: articles
#
#  id           :integer          not null, primary key
#  category_id  :integer
#  user_id      :integer
#  title        :string(255)
#  excerpt      :string(255)
#  body         :text(65535)
#  published_at :datetime
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#
# Indexes
#
#  index_articles_on_category_id  (category_id)
#  index_articles_on_user_id      (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (category_id => categories.id)
#  fk_rails_...  (user_id => users.id)
#

class Article < ApplicationRecord

  # Scopes
  default_scope { order('published_at DESC') }

  scope :draft,     -> { where(published_at: nil) }
  scope :published, -> { where.not(published_at: nil).where("published_at <= ?", Time.zone.now) }
  scope :scheduled, -> { where.not(published_at: nil).where("published_at > ?", Time.zone.now) }
  # scope :for_user,  -> (user) { where(user_id: user.id) }

  # Attr macros
  attr_accessor :status

  # AR Associations
  belongs_to :user
  belongs_to :category

  # AM Validations
  validates :title, presence: true, 
                    length: { maximum: 100 }
  validates :excerpt, presence: true,
                      length: { maximum: 255 }
  validates :body, presence: true
  validates :category_id, presence: true

  # Callbacks
  before_validation :clean_up_status

  def clean_up_status
    self.published_at = case status
                        when "Draft"
                          nil
                        when "Published"
                          Time.zone.now
                        else
                          published_at
                        end
    true
  end

  def self.archives
    Article.unscoped
          .select("YEAR(published_at) AS year, MONTHNAME(published_at) AS month, MONTH(published_at) AS month_num, COUNT(id) AS published")
          .published
          .group("year, month, month_num")
          .order("year DESC, month_num DESC")
  end

  def self.filtered (params)

    if params[:year].present? && params[:month].present?
      year = params[:year].to_i
      month = params[:month].to_i
      return where(published_at: DateTime.new(year, month, 1).beginning_of_day..DateTime.new(year, month, -1).end_of_day)
    end

    if params[:category].present?
      category_id = params[:category].to_i
      return where(category_id: category_id)
    end

    if params[:author].present?
      user_id = params[:author].to_i
      return where(user_id: user_id)
    end

    self
  end
end
