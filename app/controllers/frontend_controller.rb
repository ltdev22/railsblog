class FrontendController < ApplicationController
  layout 'frontend'
  include ArticleArchives
  include CategoriesWidget
end