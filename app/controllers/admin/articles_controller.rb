module Admin
  class ArticlesController < AdminController

    before_action :set_article, only: [:show, :edit, :update, :delete, :destroy]
    after_action :verify_authorized

    def index
      @articles = current_user.articles
                              .includes(:category)
                              .page(params[:page])
      authorize [:admin, @articles]
    end

    def show
    end

    def new
      @article = current_user.articles.build
      authorize [:admin, @article]
      categories_for_dropdown
    end

    def create
      @article = current_user.articles.build(article_params)
      authorize [:admin, @article]

      respond_to do |format|
        if @article.save
          format.html { redirect_to admin_article_path(@article), flash: { success: 'The article was successfully created' } }
        else
          categories_for_dropdown
          format.html { render 'new' }
        end
      end
    end

    def edit
      categories_for_dropdown
    end

    def update
      respond_to do |format|
        if @article.update(article_params)
          format.html { redirect_to admin_article_path(@article), flash: { success: 'The article was successfully updated' } }
        else
          categories_for_dropdown
          format.html { render 'edit' }
        end
      end
    end

    def delete
    end

    def destroy
      @article.destroy

      respond_to do |format|
        format.html { redirect_to dashboard_path, flash: { success: "The article #{ @article.title } was successfully deleted" } }
      end
    end

    private

      def set_article
        @article = Article.find(params[:id])
        authorize [:admin, @article]
      end

      def categories_for_dropdown
        @categories = Category.select(:id, :name)
      end

      def article_params
        params.require(:article).permit(
                                        :category_id, 
                                        :title, 
                                        :excerpt, 
                                        :body,
                                        :published_at,
                                        :status
                                      )
      end
  end
end