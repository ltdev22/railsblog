module ArticleArchives
  extend ActiveSupport::Concern

  included do
    before_action :include_archives
  end

  private

    def include_archives
      @archives = Article.archives
    end
end