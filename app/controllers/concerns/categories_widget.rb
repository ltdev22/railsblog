module CategoriesWidget
  extend ActiveSupport::Concern

  included do
    before_action :categories_list
  end

  private

    def categories_list
      @categories = Category.select(:id, :name)
    end
end