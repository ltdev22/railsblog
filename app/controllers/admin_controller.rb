class AdminController < ApplicationController
  before_action :authenticate_user!

  include Pundit
  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  layout 'admin'

  protected
    def user_not_authorized
      redirect_to dashboard_path, alert: "Restrict access, you don't have the privileges to access this page!"
    end
end