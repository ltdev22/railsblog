class ArticlesController < FrontendController
  def index
    @articles = Article.filtered(params)
                        .published
                        .includes(:user)
                        .includes(:category)
                        .page(params[:page])
  end

  def show
    @article = Article.published.find(params[:id])
  end
end
