$(document).on 'turbolinks:before-visit', ->
  $('.form_datetime').data('DateTimePicker').destroy();

$(document).on 'turbolinks:load', ->
  $('.form_datetime').datetimepicker({
    keepOpen: true,
    showTodayButton: true,
    widgetPositioning: {
      horizontal: 'left'
      vertical: 'bottom'
    },
    format: 'YYYY-MM-DD HH:mm',
    stepping: 5
  })
  return