changeVisibility = (status) ->
  if status == "Scheduled"
    $(".published-field").show()
  else
    $(".published-field").hide()

ready = ->
  changeVisibility $("#article_status :selected").text()
  $("#article_status").on "change", (e) ->
    changeVisibility $(this).find(":selected").text()

$(document).on 'turbolinks:load', ready
