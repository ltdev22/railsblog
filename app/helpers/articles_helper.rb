module ArticlesHelper
  def status_for(article)
    if article.published_at?
      if article.published_at > Time.zone.now
        "Scheduled"
      else
        "Published"
      end
    else
      "Draft"
    end
  end

  def status_label_for(article)
    if article.published_at?
      if article.published_at > Time.zone.now
        content_tag :span, "Scheduled", class: "label label-warning"
      else
        content_tag :span, "Published", class: "label label-success"
      end
    else
      content_tag :span, "Draft", class: "label label-danger"
    end
  end
end
