module Admin
  class ArticlePolicy < AdminPolicy

    def index?
      true
    end

    def show?
      scope.where(:id => article.id).exists? && owned_by_user
    end

    def create?
      true
    end

    def update?
      owned_by_user
    end

    def destroy?
      owned_by_user
    end

    def delete?
      destroy?
    end

    private

      def article
        record
      end

      def owned_by_user
        user.present? && user == article.user
      end
  end
end
