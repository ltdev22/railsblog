# RailsBlog (demo project)

This is a blog - magazine developed with [Ruby on Rails](http://rubyonrails.org/) framework. 

### Description

There's a frontend and backend/admin section on this project.

On front: Listing all Articles and viewing a single one.

On admin: Full CRUD functionality for Articles. The author can manage only his own articles.

The project contains tests.

### Instructions

* Clone the repository on your local machine

* Run the `bundle` command through your terminal

* Run `rails db:migrate` to run all migrations

* Run `rails db:seed` if you want to seed some data. This is optional of course
