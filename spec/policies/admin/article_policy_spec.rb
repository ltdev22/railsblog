require 'rails_helper'
require 'pundit/matchers'

RSpec.describe Admin::ArticlePolicy do

  subject { described_class.new(user, [:admin, article]) }
  let(:user) { FactoryGirl.create(:user) }

  context 'permit authorization' do
    let(:article) { FactoryGirl.create(:article, user_id: user.id) }
    
    it { is_expected.to permit_action(:edit) }
    it { is_expected.to permit_action(:delete) }
  end
end
