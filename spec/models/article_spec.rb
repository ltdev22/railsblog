# == Schema Information
#
# Table name: articles
#
#  id           :integer          not null, primary key
#  category_id  :integer
#  user_id      :integer
#  title        :string(255)
#  excerpt      :string(255)
#  body         :text(65535)
#  published_at :datetime
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#
# Indexes
#
#  index_articles_on_category_id  (category_id)
#  index_articles_on_user_id      (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (category_id => categories.id)
#  fk_rails_...  (user_id => users.id)
#

require 'rails_helper'

RSpec.describe Article, type: :model do

  it 'has a valid factory' do
    expect(build(:article)).to be_valid
  end

  let(:article) { build(:article) }

  describe 'ActiveModel validations' do
    it { expect(article).to validate_presence_of(:title) }
    it { expect(article).to validate_length_of(:title).is_at_most(100) }

    it { expect(article).to validate_presence_of(:excerpt) }
    it { expect(article).to validate_length_of(:excerpt).is_at_most(255) }

    it { expect(article).to validate_presence_of(:body) }
  end

  describe 'ActiveRecord associations' do
    it { expect(article).to belong_to(:user) }
    it { expect(article).to belong_to(:category) }
  end

  describe 'Scopes' do
    let!(:draft_post) { FactoryGirl.create(:article, published_at: nil) }
    let!(:published_post) { FactoryGirl.create(:article, published_at: Time.zone.now) }
    let!(:scheduled_post) { FactoryGirl.create(:article, published_at: 10.days.from_now) }
    
    it 'sorts articles by most recent by default' do
      expect(Article.all.to_sql).to eq Article.order('published_at DESC').all.to_sql
    end

    context '#draft' do
      it 'returns a draft post' do
        expect(Article.draft).to include draft_post
      end

      it 'does not include undraft posts' do
        expect(Article.draft).to_not include(published_post, scheduled_post)
      end
    end

    context '#published' do
      it 'returns a published post' do
        expect(Article.published).to include published_post
      end

      it 'does not include unpublished posts' do
        expect(Article.published).to_not include(draft_post, scheduled_post)
      end
    end

    context '#scheduled' do
      it 'returns a scheduled post' do
        expect(Article.scheduled).to include scheduled_post
      end

      it 'does not include unscheduled posts' do
        expect(Article.scheduled).to_not include(draft_post, published_post)
      end
    end
  end # end describe 'scopes'

  describe '.archives' do
    let!(:first) { FactoryGirl.create(:article, published_at: Time.zone.now) }
    let!(:second) { FactoryGirl.create(:article, published_at: 1.month.ago) }

    it 'returns articles archived' do
      expected_total = 1
      actual_archives = Article.archives.map { |article| [article.year, article.month, article.published] }
      expected_archives = [first, second].map { |article| [article.published_at.year, article.published_at.strftime("%B"), expected_total] }

      expect(actual_archives).to match_array(expected_archives)
    end
  end
  
  describe '.filtered' do
    let!(:first) { FactoryGirl.create(:article, title: 'First article', published_at: Time.zone.now) }
    let!(:second) { FactoryGirl.create(:article, title: 'Second article', published_at: 1.month.ago) }

    it 'is within specific year and month' do
      year = Date.today.strftime("%Y")
      month = Date.today.strftime("%m")
      expect(Article.filtered({year: year, month: month})).to include(first)
      expect(Article.filtered({year: year, month: month})).to_not include(second)
    end

    it 'lists from specific category' do
      expect(Article.filtered(category: 1)).to include(first)
      expect(Article.filtered(category: 1)).to_not include(second)
    end

    it 'lists from specific author' do
      expect(Article.filtered(author: 1)).to include(first)
      expect(Article.filtered(author: 1)).to_not include(second)
    end

  end
end
