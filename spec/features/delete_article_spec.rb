require 'rails_helper'

RSpec.feature 'Deleting an existing article', type: :feature do
  background do
    @author = FactoryGirl.create(:user)
    @article = FactoryGirl.create(:article, user_id: @author.id)
    
    sign_in_as @author
    click_link 'My Dashboard'
  end

  scenario 'Delete an article successfully' do
    click_link('Delete', href: delete_admin_article_path(@article))
    click_button 'Yes, Delete it!'
    expect(page).to have_content("The article #{ @article.title } was successfully deleted")
    expect(page).not_to have_link(@article.title)
    expect(page.current_path).to eq(dashboard_path)
  end
end