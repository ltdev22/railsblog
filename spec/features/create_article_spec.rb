require 'rails_helper'

RSpec.feature 'Creating new article', type: :feature do
  background do
    @author = FactoryGirl.create(:user)
    sign_in_as @author
    click_link 'My Dashboard'
    click_link 'New Article'
  end

  scenario 'Create a new valid article' do
    fill_in 'Title', with: 'Hello World! This is the first article!'
    select 'Published', :from => 'Status'
    fill_in 'Published at', with: Time.now
    fill_in 'Excerpt',  with: 'Hello World! This is the first article excerpt!'
    fill_in 'Body',  with: 'Hello World! This is the first article body!'
    click_button 'Create Article'

    expect(page).to have_content('The article was successfully created')
    expect(page).to have_content('Hello World! This is the first article!')
  end

  scenario 'Cannot create a new article when validation fails' do
    fill_in 'Title', with: 'Hello World! ' * 50
    fill_in 'Excerpt', with: 'Hello World! ' * 100
    fill_in 'Body', with: ''
    click_button 'Create Article'

    expect(page).to have_content('Please review the problems below')
    expect(page).to have_content('Title is too long (maximum is 100 characters)')
    expect(page).to have_content('Excerpt is too long (maximum is 255 characters)')
    expect(page).to have_content('Body can\'t be blank')
  end
end