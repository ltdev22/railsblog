require 'rails_helper'

RSpec.feature 'Editing an existing article', type: :feature do
  background do
    @author = FactoryGirl.create(:user)
    article = create(:article, user_id: @author.id)
    
    sign_in_as @author
    click_link 'My Dashboard'
    click_link('Edit', href: edit_admin_article_path(article))
  end

  scenario 'Edit a valid article' do
    fill_in 'Title', with: 'This is an updated article!'
    fill_in 'Body', with: 'This is an updated article content.'
    click_button 'Update Article'

    expect(page).to have_content('The article was successfully updated')
    expect(page).to have_content('This is an updated article!')
  end

  scenario 'Cannot edit an article when validation fails' do
    fill_in 'Title', with: 'update failed  ' * 100
    click_button 'Update Article'

    expect(page).to have_content('Please review the problems below')
    expect(page).to have_content('Title is too long (maximum is 100 characters)')
  end
end