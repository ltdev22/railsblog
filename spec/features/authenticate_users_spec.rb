require 'rails_helper'

RSpec.feature 'Authenticate Users', type: :feature do

  describe 'Creating new account' do
    background do
      @user = FactoryGirl.build(:user)
      
      visit '/'
      expect(page).not_to have_content('My Dashboard')
      click_link 'Sign Up'
    end

    scenario 'User can sign up successfully' do
      fill_in 'First name', with: @user.first_name
      fill_in 'Last name', with: @user.last_name
      fill_in 'Email', with: @user.email
      fill_in 'Password', with: @user.password, match: :first
      fill_in 'Password confirmation', with: @user.password
      click_button 'Sign up'

      expect(page).not_to have_content('Sign In')
      expect(page).not_to have_content('Sign Up')
      expect(page).to have_content('My Dashboard')
      expect(page).to have_content('Welcome! You have signed up successfully')
    end
  end

  describe 'User session' do
    background do
      @user = FactoryGirl.create(:user)
      
      visit '/'
      expect(page).not_to have_content('My Dashboard')
      click_link 'Sign In'
      
      fill_in 'Email', with: @user.email
      fill_in 'Password', with: @user.password
      click_button 'Log in'
    end

    scenario 'User can sign in successfully' do
      expect(page).not_to have_content('Sign In')
      expect(page).not_to have_content('Sign Up')
      expect(page).to have_content('My Dashboard')
      expect(page).to have_content('Signed in successfully')
    end

    scenario 'User can sign out successfully' do
      click_link 'Log Out'
      expect(page).not_to have_content('My Dashboard')
      expect(page).to have_content('Sign In')
      expect(page).to have_content('Sign Up')
    end
  end
end