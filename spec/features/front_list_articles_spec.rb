require 'rails_helper'

RSpec.feature 'Listing all articles', type: :feature do
  scenario 'List all articles on homepage' do
    first_article = create(:article, title: 'This is the first article')
    second_article = create(:article, title: 'This is the second article')
    hidden_article = create(:article, title: 'This is a hidden article', published_at: 10.days.from_now)
    
    visit '/'

    expect(page).to have_content('This is the first article')
    expect(page).to have_content('This is the second article')
    expect(page).not_to have_content('This is a hidden article')
  end
end