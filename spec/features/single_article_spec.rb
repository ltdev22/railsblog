require 'rails_helper'

RSpec.feature 'Display individual articles', type: :feature do
  scenario 'View a single article' do
    article = create(:article)
    
    visit '/'
    find(".blog-post-title > a[href='#{article_path(article)}']").click

    expect(page).to have_content(article.title)
    expect(page.current_path).to eq(article_path(article))
  end
end