require 'rails_helper'

RSpec.feature 'Dashboard', type: :feature do
  scenario 'List all articles on dashboard' do
    author = FactoryGirl.create(:user)
    another_author = FactoryGirl.create(:user)
    
    first_article = create(:article, title: 'This is the first article', user_id: author.id)
    second_article = create(:article, title: 'This is the second article', user_id: author.id)
    third_article = create(:article, title: 'This is the third article', user_id: another_author.id)
    
    sign_in_as author
    click_link 'My Dashboard'

    expect(page).to have_content('This is the first article')
    expect(page).to have_content('This is the second article')
    expect(page).not_to have_content('This is the third article')
  end
end