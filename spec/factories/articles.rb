# == Schema Information
#
# Table name: articles
#
#  id           :integer          not null, primary key
#  category_id  :integer
#  user_id      :integer
#  title        :string(255)
#  excerpt      :string(255)
#  body         :text(65535)
#  published_at :datetime
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#
# Indexes
#
#  index_articles_on_category_id  (category_id)
#  index_articles_on_user_id      (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (category_id => categories.id)
#  fk_rails_...  (user_id => users.id)
#

FactoryGirl.define do
  factory :article do
    title 'This is the article title'
    excerpt 'This is the article excerpt'
    body 'This is the article body'
    published_at Time.now
    association :user
    association :category
  end
end
