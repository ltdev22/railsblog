# == Route Map
#
#                   Prefix Verb   URI Pattern                          Controller#Action
#         new_user_session GET    /login(.:format)                     devise/sessions#new
#             user_session POST   /login(.:format)                     devise/sessions#create
#     destroy_user_session DELETE /logout(.:format)                    devise/sessions#destroy
#        new_user_password GET    /password/new(.:format)              devise/passwords#new
#       edit_user_password GET    /password/edit(.:format)             devise/passwords#edit
#            user_password PATCH  /password(.:format)                  devise/passwords#update
#                          PUT    /password(.:format)                  devise/passwords#update
#                          POST   /password(.:format)                  devise/passwords#create
# cancel_user_registration GET    /cancel(.:format)                    devise/registrations#cancel
#    new_user_registration GET    /register(.:format)                  devise/registrations#new
#   edit_user_registration GET    /edit(.:format)                      devise/registrations#edit
#        user_registration PATCH  /                                    devise/registrations#update
#                          PUT    /                                    devise/registrations#update
#                          DELETE /                                    devise/registrations#destroy
#                          POST   /                                    devise/registrations#create
#                 articles GET    /articles(.:format)                  articles#index
#                  article GET    /articles/:id(.:format)              articles#show
#     delete_admin_article GET    /admin/articles/:id/delete(.:format) admin/articles#delete
#           admin_articles POST   /admin/articles(.:format)            admin/articles#create
#        new_admin_article GET    /admin/articles/new(.:format)        admin/articles#new
#       edit_admin_article GET    /admin/articles/:id/edit(.:format)   admin/articles#edit
#            admin_article GET    /admin/articles/:id(.:format)        admin/articles#show
#                          PATCH  /admin/articles/:id(.:format)        admin/articles#update
#                          PUT    /admin/articles/:id(.:format)        admin/articles#update
#                          DELETE /admin/articles/:id(.:format)        admin/articles#destroy
#                dashboard GET    /dashboard(.:format)                 admin/articles#index
#                     root GET    /                                    articles#index
# 

Rails.application.routes.draw do
  
  devise_for :users, path: '', path_names: { sign_in: 'login', sign_out: 'logout', sign_up: 'register' }
  resources :articles, only: [:index, :show]

  namespace :admin do
    resources :articles, except: [:index] do
      get :delete, on: :member
    end
  end
  get 'dashboard', to: 'admin/articles#index', as: :dashboard

  root 'articles#index'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
