class AddPublishedAtOnArticles < ActiveRecord::Migration[5.0]
  def change
    add_column :articles, :published_at, :datetime, after: :body
  end
end
