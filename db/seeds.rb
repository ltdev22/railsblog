# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

require 'faker'
require 'database_cleaner'

puts 'Truncate tables'
DatabaseCleaner.clean_with(:truncation)

puts 'Start seeding ...'

user = User.create(
  first_name: 'Demo',
  last_name: 'Demo',
  email: 'demo@test.com',
  password: 'password123'
)

3.times do |user|
  user = User.create(
    first_name: Faker::Name.first_name,
    last_name: Faker::Name.last_name,
    email: Faker::Internet.free_email,
    password: 'password123'
  )
end

puts '1+3 users have been created successfully ...'

Category.create([
  { name: 'News' }, 
  { name: 'Sports' }, 
  { name: 'Lifestyle' }, 
  { name: 'Culture' }, 
  { name: 'Tech' }, 
])

puts '5 categories have been created successfully ...'

20.times do |article|
  article = Article.create(
    user_id: Faker::Number.between(1, 4),
    category_id: Faker::Number.between(1, 5),
    title: Faker::Lorem.sentence,
    excerpt: Faker::Lorem.paragraph(2),
    body: Faker::Lorem.paragraph(50),
    published_at: Faker::Time.between(3.months.ago, Date.today, :all)
  )
  article.update_attribute :created_at, Faker::Time.between(3.months.ago, Date.today, :all)
end

puts '20 articles have been created successfully ...'

puts 'Seeding completed!'
